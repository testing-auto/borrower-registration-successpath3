package com.lendenclub.test.main;

import org.testng.annotations.Test;
import java.io.IOException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.lendenclub.supporting.classes.TestListener;
import com.lendenclub.test.AdharCard;
import com.lendenclub.test.AssessmentFee;
import com.lendenclub.test.LiveKyc;
import com.lendenclub.test.LoanRequirement;
import com.lendenclub.test.LocationAndCreditDetails;
import com.lendenclub.test.ProfessionalDetails;
import com.lendenclub.test.SocialSignUp;
import com.lendenclub.test.SubmitBankStatement;
import com.lendenclub.test.UploadEnach;
import jxl.read.biff.BiffException;

@Listeners(TestListener.class)
public class BorrowerRegistration extends Capabilities{
	SocialSignUp sign = new SocialSignUp();
	ProfessionalDetails professional = new ProfessionalDetails();
	AdharCard adhar = new AdharCard();
	AssessmentFee fee = new AssessmentFee();
	LiveKyc kyc = new LiveKyc();
	LoanRequirement loan = new LoanRequirement();
	LocationAndCreditDetails location = new LocationAndCreditDetails();
	SubmitBankStatement bank = new SubmitBankStatement();
	UploadEnach nach = new UploadEnach();

	@BeforeTest
	public void mySetUp() throws BiffException, IOException {
		setUp();
	}

	@Test(priority = 1)
	public void test001SocialSignUp(){
		sign.page001SocialSignUp();
		System.out.println("This is test application");
	}

	@Test(priority = 2)
	public void test002ProfessionalDetails(){
		professional.page002ProfessionalDetails();
	}

	@Test(priority = 3)
	public void test003LocationAndCreditDetails() {
		location.page003LocationAndCreditDetails();
	}

	@Test(priority = 4)
	public void test004LoanRequirement() {
		loan.page004LoanRequirement();
	}

	@Test(priority = 5)
	public void test005AssessmentFee() {
		fee.page005AssessmentFee();
	}

	@Test(priority = 6)
	public void test006LiveKyc() {
		kyc.page006LiveKyc();
	}

	@Test(priority = 7)
	public void test007AdharCard() {
		adhar.page007AdharCard();
	}

	@Test(priority = 8)
	public void test008SubmitBankStatement() {
		bank.page008SubmitBankStatement();
	}

	@Test(priority = 9)
	public void test009Enach() {
		nach.page009Enach();
	}

	@AfterTest
	public void tearDown() {
		driver.closeApp();
	}

}

