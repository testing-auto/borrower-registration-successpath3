package com.lendenclub.test.main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.lendenclub.supporting.classes.ExcelSheetLibrary;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import jxl.read.biff.BiffException;

public class Capabilities {
	public static AndroidDriver<MobileElement> driver;
	// AppiumDriver<MobileElement> driver;
	// private ExcelSheetLibrary excel;

	public static AndroidDriver<MobileElement> getDriver() {
		return driver;
	}

	public static void setDriver(AndroidDriver<MobileElement> driver) {
		Capabilities.driver = driver;
	}

	public void setUp() throws BiffException, IOException {

		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
//			caps.setCapability(MobileCapabilityType.VERSION, "11 RP1A.200720.011");
//			caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Redmi Note 8 Pro");
//			caps.setCapability(MobileCapabilityType.UDID, "79xsovx4ojbihmfu");
			caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".MainActivity");
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");
			
			caps.setCapability(MobileCapabilityType.APP,
					System.getProperty("user.dir")+"/src/test/resources/apps/app-qa.apk");
			URL url = new URL("http://localhost:4723/wd/hub");
			driver = new AndroidDriver<MobileElement>(url, caps);
			ExcelSheetLibrary excel = new ExcelSheetLibrary(
					"./src/test/resources/apps/test1.xls");
			
			// TO Clear Source Folder ScreenShot
			File source = new File("./Screenshot");
			FileUtils.cleanDirectory(source);
		} catch (Exception exp) {
			exp.printStackTrace();
		}

	}

}
